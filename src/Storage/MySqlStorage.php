<?php


namespace App\Storage;

use App\Storage\MySqlConnection;

class MySqlStorage implements StorageInterface
{
    protected \PDO $connection;

    public function __construct()
    {
        //Set connection
        $this->connection = MySqlConnection::getConnection();
    }

    //Get records from table by id
    public function find(string $table, int $id)
    {
        $sql = 'SELECT * FROM ' . $table . ' WHERE id = :id';
        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue(':id', $id);
        $stmt->execute();

        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    //Create records in table from data
    public function create(string $table, array $data)
    {
        $sql = 'INSERT INTO ' . $table;
        $sql .= ' (' . implode(', ', array_keys($data)) . ') ';
        $sql .= 'VALUES (:' . implode(', :', array_keys($data)) . ')';

        $stmt = $this->connection->prepare($sql);

        foreach ($data as $field => $value){
            if($value === null){
                $stmt->bindValue(':' . $field, $value, \PDO::PARAM_NULL);
                continue;
            }
            $stmt->bindValue(':' . $field, $value);
        }

        $stmt->execute();

        return $this->connection->lastInsertId();
    }

    //Update records in table by id from data
    public function update(string $table, string $idName, array $data)
    {
        //Сut the identifier from the data array
        $id = $data[$idName];
        unset($data[$idName]);

        //Preparation fields for sql
        $field_preparation = [];
        foreach (array_keys($data) as $key){
            $field_preparation[] = $key . ' = :' . $key;
        }

        $sql = 'UPDATE ' . $table . ' SET ';
        $sql .= implode(', ', $field_preparation);
        $sql .= ' WHERE ' . $idName . ' = :' . $idName ;

        $stmt = $this->connection->prepare($sql);

        foreach ($data as $field => $value){
            if($value === null){
                $stmt->bindValue(':' . $field, $value, \PDO::PARAM_NULL);
                continue;
            }
            $stmt->bindValue(':' . $field, $value);
        }
        $stmt->bindValue(':' . $idName, $id);

        $stmt->execute();
    }

    //Delete records from table by id
    public function delete(string $table, int $id)
    {
        $sql = 'DELETE FROM ' . $table . ' WHERE id = :id';
        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    }
}