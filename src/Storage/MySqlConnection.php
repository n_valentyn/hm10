<?php


namespace App\Storage;

/*
 * Connection to database MySQL
 */
class MySqlConnection
{
    protected static \PDO $connection;

    public static function getConnection() {
        if(empty(self::$connection)) {
            $db_info = array(
                "db_host" => "mysql",
                "db_port" => "",
                "db_user" => "root",
                "db_pass" => "password",
                "db_name" => "docker");
            try {
                self::$connection = new \PDO("mysql:host=".$db_info['db_host'].';port='.$db_info['db_port'].';dbname='.$db_info['db_name'], $db_info['db_user'], $db_info['db_pass']);
            } catch(PDOException $error) {
                echo $error->getMessage();
            }
        }
        return self::$connection;
    }

}