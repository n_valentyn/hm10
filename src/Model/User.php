<?php

namespace App\Model;

use App\Model\Model;

class User extends Model
{
    protected int $id;
    protected string $name;
    protected string $email;
    protected string $password;

    //We can set custom value:
    //protected static string $table = 'users';
    //protected static string $primary_key = 'id';
}