<?php
include_once '../vendor/autoload.php';

use App\Model\User;

//Creat
$user = new User();
$user->setName('Jon');
$user->setEmail('jon@gmail.com');
$user->setPassword('jhyufds76HG');

//OR
//$user = new User([
//    'name' => 'Julia',
//    'email' => 'julia@gmail.com',
//    'password' => 'qwerty'
//]);

$user->create();

echo "User object: <br>";
var_dump($user);

$id = $user->getId();
unset($user);

//Find
$user = User::find($id);

echo "Test Model getters: <br><br>";
echo $user->getId();
echo '<br>';
echo $user->getName();
echo '<br>';
echo $user->getEmail();
echo '<br>';
echo $user->getPassword();

//Update
$user->setName('Jon Paterson');
$user->setEmail('jon.paterson@gmail.com');
$user->update();
unset($user);

$user = User::find($id);
var_dump($user);

//Delete
//$user->delete($id);

